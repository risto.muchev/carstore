package com.sat.carstore.demo.controller.exception;

public class EmailAlreadyUsedException extends Exception {

    private static final long serialVersionUID = 1L;

    public EmailAlreadyUsedException() {
        super("Email is already in use!");
    }
}
