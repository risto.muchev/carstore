package com.sat.carstore.demo.controller;

import com.sat.carstore.demo.controller.dto.PasswordStoreUserDTO;
import com.sat.carstore.demo.controller.dto.StoreUserDTO;
import com.sat.carstore.demo.domain.StoreUser;
import com.sat.carstore.demo.service.StoreUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the {@link StoreUser} entity, and needs to fetch its collection of authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class StoreUserController {

    private final StoreUserService userService;

    public StoreUserController(StoreUserService userService) {
        this.userService = userService;
    }

    @PostMapping("/create-user")
    public ResponseEntity createUser(@Valid @RequestBody PasswordStoreUserDTO storeUserDTO) throws Exception {
        log.debug("REST request to create User : {}", storeUserDTO);
        try {
            StoreUser newUser = userService.createUser(storeUserDTO);
            return ResponseEntity.ok(newUser);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

    @PostMapping("/batch-create-users")
    public ResponseEntity createUser(@Valid @RequestBody List<PasswordStoreUserDTO> storeUserDTOList) throws Exception {
        log.debug("REST request to create User List : {}", storeUserDTOList);
        try {
            List<StoreUser> newUsers = userService.batchCreateUsers(storeUserDTOList);
            return ResponseEntity.ok(newUsers);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

    @GetMapping("/list-users")
    public ResponseEntity<List<StoreUserDTO>> getAllUsers() {
        log.debug("REST request to get all User for an admin");

        List<StoreUserDTO> users = userService.getAllUsersDTO();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

}
