package com.sat.carstore.demo.controller.dto;

import com.sat.carstore.demo.domain.Car;
import com.sat.carstore.demo.domain.CarBrand;
import com.sat.carstore.demo.domain.CarCategory;
import com.sat.carstore.demo.domain.StoreUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A CarDTO.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @Size(min = 2)
    private String name;

    private String category;

    private String brand;

    private String description;

    private Double price;

    private String manufacturingDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CarDTO)) {
            return false;
        }

        CarDTO carDTO = (CarDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, carDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    public static CarDTO from(Car car){
        CarDTO carDTO = new CarDTO();
        carDTO.setId(car.getId());
        carDTO.setName(car.getName());
        carDTO.setBrand(car.getBrand().name());
        carDTO.setCategory(car.getCategory().name());
        carDTO.setDescription(car.getDescription());
        carDTO.setPrice(car.getPrice());
        carDTO.setManufacturingDate(car.getManufacturingDate().toString());
        return carDTO;
    }

}
