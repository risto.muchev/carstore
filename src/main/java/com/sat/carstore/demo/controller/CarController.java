package com.sat.carstore.demo.controller;

import com.sat.carstore.demo.controller.dto.CarDTO;
import com.sat.carstore.demo.domain.Car;
import com.sat.carstore.demo.domain.StoreUser;
import com.sat.carstore.demo.service.CarService;
import com.sat.carstore.demo.service.StoreUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@Slf4j
public class CarController {

    private final CarService carService;
    private final StoreUserService userService;

    public CarController(CarService carService, StoreUserService userService) {
        this.carService = carService;
        this.userService = userService;
    }

    @PostMapping("/create-car")
    public ResponseEntity createCar(@Valid @RequestBody CarDTO carDTO) throws Exception {
        log.debug("REST request to create Car : {}", carDTO);

        try {
            Car newCar = carService.createCar(carDTO);
            return ResponseEntity.ok(newCar);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/batch-create-cars")
    public ResponseEntity batchCreateCar(@Valid @RequestBody List<CarDTO> carDTOList) throws Exception {
        log.debug("REST request to create Car : {}", carDTOList);

        try {
            List<Car> newCars = carService.createCars(carDTOList);
            return ResponseEntity.ok(newCars);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/list-cars")
    public ResponseEntity<List<Car>> getAllCars() {
        log.debug("REST request to get all Car for an admin");

        List<Car> cars = carService.getAllCars();
        return new ResponseEntity<>(cars, HttpStatus.OK);
    }

    @PutMapping("/buy-car")
    public ResponseEntity buy(@Valid @RequestBody CarDTO carDTO){
        log.debug("REST request to buy Car : {}", carDTO);

        try {
            StoreUser storeUser = userService.getUserWithAuthority().orElse(null);
            Car updatedCar = carService.buyCar(carDTO, storeUser);
            return ResponseEntity.ok(updatedCar);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/history-log")
    public ResponseEntity<List<Car>> getHistoryLog() {
        log.debug("REST request to get all Car for an admin");

        List<Car> cars = carService.getHistoryLog();
        return new ResponseEntity<>(cars, HttpStatus.OK);
    }
}
