package com.sat.carstore.demo.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PasswordStoreUserDTO extends StoreUserDTO{
    @Size(min = 60, max = 60)
    private String password;
}
