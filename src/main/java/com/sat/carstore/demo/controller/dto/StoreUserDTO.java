package com.sat.carstore.demo.controller.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sat.carstore.demo.domain.Authority;
import com.sat.carstore.demo.domain.StoreUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authority.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreUserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    private Double budget;

    private String authority;

    public StoreUserDTO(StoreUser user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.budget = user.getBudget();
        this.authority = user.getAuthority().getName();
    }
}
