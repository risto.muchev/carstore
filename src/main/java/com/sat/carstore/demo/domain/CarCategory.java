package com.sat.carstore.demo.domain;

public enum CarCategory {
    SUV, SEDAN, HATCHBACK, LIMO
}
