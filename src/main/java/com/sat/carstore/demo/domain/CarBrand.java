package com.sat.carstore.demo.domain;

public enum CarBrand {
    BMW, MERCEDES, FIAT, OPEL, VW
}
