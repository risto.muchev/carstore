package com.sat.carstore.demo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Slf4j
public class NotificationService {
    public void sendEmail(String to, String subject, String body){
        log.info("Sent email to {} with subject {} and body {}", to, subject, body);
    }
}
