package com.sat.carstore.demo.service;

import com.sat.carstore.demo.controller.dto.CarDTO;
import com.sat.carstore.demo.domain.Car;
import com.sat.carstore.demo.domain.CarBrand;
import com.sat.carstore.demo.domain.CarCategory;
import com.sat.carstore.demo.domain.StoreUser;
import com.sat.carstore.demo.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@Slf4j
public class CarService {

    private final CarRepository carRepository;
    private final StoreUserService userService;
    private final NotificationService notificationService;

    public CarService(CarRepository carRepository, StoreUserService userService, NotificationService notificationService){
        this.carRepository = carRepository;
        this.userService = userService;
        this.notificationService = notificationService;
    }

    @Transactional(readOnly = true)
    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<Car> getAllAvalilableCars() {
        return carRepository.findAllByOwnerNull();
    }

    @Transactional(readOnly = true)
    public List<Car> getHistoryLog() {
        return carRepository.findAllByOwnerNotNullOrderByBuyDateDesc();
    }

    public Car createCar(CarDTO carDTO) throws Exception{
        validate(carDTO);
        Car car = from(carDTO);
        carRepository.save(car);
        log.debug("Created Information for Car: {}", car);
        return car;
    }

    private void validate(CarDTO carDTO) throws Exception{
        if (carDTO.getId() != null) {
            throw new Exception("A new user cannot already have an ID");
        }
    }

    private Car from(CarDTO carDTO){
        Car car = new Car();
        car.setBrand(CarBrand.valueOf(carDTO.getBrand()));
        car.setCategory(CarCategory.valueOf(carDTO.getCategory()));
        car.setName(carDTO.getName());
        car.setDescription(carDTO.getDescription());
        Date date = Date.valueOf(carDTO.getManufacturingDate());
        car.setManufacturingDate(date);

        //pricing calculator
        int priceCoeff = 1;
        priceCoeff+=car.getBrand().ordinal();
        priceCoeff+=car.getCategory().ordinal();

        //older means cheaper
        Date now = new Date(Instant.now().toEpochMilli());
        long old = now.getTime() - date.getTime();
        long years5milis = 5L * 365 * 24 * 60 * 60 * 1000;
        long years10milis = 10L * 365 * 24 * 60 * 60 * 1000;
        if(old < years5milis){
            priceCoeff+=2;
        }else if(old < years10milis){
            priceCoeff+=1;
        }

        car.setPrice(Math.random() * 1000 * priceCoeff);
        car.setOwner(null);
        car.setCreatedBy(userService.getUserWithAuthority().orElse(null));
        return car;
    }

    public List<Car> createCars(List<CarDTO> carDTOList) throws Exception{
        List<Car> carList = new ArrayList<>();
        for(CarDTO carDTO:carDTOList){
            validate(carDTO);
            Car car = from(carDTO);
            carList.add(car);
        }
        carRepository.saveAll(carList);
        log.debug("Created Information for Car List: {}", carList);
        return carList;
    }

    public Car buyCar(CarDTO carDTO, StoreUser storeUser) throws Exception{
        if (carDTO.getId() == null) {
            throw new Exception("Buy-ing a car must have an ID");
        }

        Car car = carRepository.findById(carDTO.getId()).orElse(null);
        if(car == null){
            throw new Exception("The car id does not exists!");
        }

        if(car.getOwner()!=null){
            throw new Exception("The car already has an owner!");
        }

        if(storeUser == null){
            throw new Exception("Problem while getting logged in user!");
        }

        if(storeUser.getBudget() < car.getPrice()){
            throw new Exception("You do not have enough money to buy this car!");
        }

        buy(car, storeUser);

        return car;
    }

    private void buy(Car car, StoreUser storeUser){
        car.setOwner(storeUser);
        car.setBuyDate(new Date(Instant.now().toEpochMilli()));

        carRepository.save(car);
        log.debug("Updated Information for Car: {}", car);

        //decrease user budget
        storeUser.setBudget(storeUser.getBudget() - car.getPrice());
        userService.save(storeUser);
        log.debug("Updated budget for User: {}", storeUser.getEmail());

        notificationService.sendEmail(storeUser.getEmail(), "You bought a car", car.toString());

    }

    //every one hour
    @Scheduled(cron = "0 0 * * * *")
    @Transactional
    public void buyAll(){
        List<StoreUser> users = userService.getAllUsers();
        List<Car> cars = getAllAvalilableCars();

        //first come first served - greedy
        for(Car car:cars){
            for(StoreUser storeUser: users){
               if(storeUser.getBudget() > car.getPrice()){
                   buy(car, storeUser);
               }
            }
        }
    }
}
