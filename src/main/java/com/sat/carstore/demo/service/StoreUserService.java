package com.sat.carstore.demo.service;

import com.sat.carstore.demo.controller.dto.PasswordStoreUserDTO;
import com.sat.carstore.demo.controller.dto.StoreUserDTO;
import com.sat.carstore.demo.controller.exception.EmailAlreadyUsedException;
import com.sat.carstore.demo.domain.StoreUser;
import com.sat.carstore.demo.repository.AuthorityRepository;
import com.sat.carstore.demo.repository.StoreUserRepository;
import com.sat.carstore.demo.security.AuthoritiesConstants;
import com.sat.carstore.demo.security.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
@Slf4j
public class StoreUserService {

    private final StoreUserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    public StoreUserService(StoreUserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
    }

    public StoreUser createUser(PasswordStoreUserDTO storeUserDTO) throws Exception{
        validate(storeUserDTO);
        StoreUser user = from(storeUserDTO);
        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    private void validate(PasswordStoreUserDTO storeUserDTO) throws Exception{
        if (storeUserDTO.getAuthority().equalsIgnoreCase(AuthoritiesConstants.ADMIN)) {
            if (!SecurityUtils.hasCurrentUserThisAuthority(AuthoritiesConstants.ADMIN)) {
                throw new Exception("Only ADMIN user can create ADMIN user");
            }
        }
        if (storeUserDTO.getId() != null) {
            throw new Exception("A new user cannot already have an ID");
            // Lowercase the user login before comparing with database
        }else if (userRepository.findOneByEmailIgnoreCase(storeUserDTO.getEmail()).isPresent()) {
            throw new EmailAlreadyUsedException();
        }
    }

    private StoreUser from(PasswordStoreUserDTO storeUserDTO) {
        StoreUser user = new StoreUser();
        user.setFirstName(storeUserDTO.getFirstName());
        user.setLastName(storeUserDTO.getLastName());
        user.setBudget(storeUserDTO.getBudget());
        if (storeUserDTO.getEmail() != null) {
            user.setEmail(storeUserDTO.getEmail().toLowerCase());
        }
        String encryptedPassword = passwordEncoder.encode(storeUserDTO.getPassword());
        user.setPassword(encryptedPassword);
        if (storeUserDTO.getAuthority() != null) {
            user.setAuthority(authorityRepository.findById(storeUserDTO.getAuthority()).orElse(null));
        }
        return user;
    }

    @Transactional(readOnly = true)
    public List<StoreUserDTO> getAllUsersDTO() {
        return userRepository.findAll().stream().map(StoreUserDTO::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<StoreUser> getAllUsers() {
        return userRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<StoreUser> getUserWithAuthority() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneByEmailIgnoreCase);
    }

    public StoreUser save(StoreUser user){
        return userRepository.save(user);
    }

    public List<StoreUser> batchCreateUsers(List<PasswordStoreUserDTO> storeUserDTOList) throws Exception{
        List<StoreUser> list = new ArrayList<>();
        for(PasswordStoreUserDTO storeUserDTO:storeUserDTOList) {
            validate(storeUserDTO);
            StoreUser user = from(storeUserDTO);
            list.add(user);
        }
        userRepository.saveAll(list);
        log.debug("Created Information for User List: {}", list);
        return list;
    }
}
