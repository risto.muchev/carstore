package com.sat.carstore.demo.repository;

import com.sat.carstore.demo.domain.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data JPA repository for the Car entity.
 */
@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
    List<Car> findAllByOwnerNotNullOrderByBuyDateDesc();
    List<Car> findAllByOwnerNull();

}
