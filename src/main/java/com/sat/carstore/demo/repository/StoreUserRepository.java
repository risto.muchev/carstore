package com.sat.carstore.demo.repository;


import com.sat.carstore.demo.domain.StoreUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the {@link StoreUser} entity.
 */
@Repository
public interface StoreUserRepository extends JpaRepository<StoreUser, Long> {
    Optional<StoreUser> findOneByEmailIgnoreCase(String email);


}
