package com.sat.carstore.demo.config;

import com.sat.carstore.demo.security.AuthoritiesConstants;
import com.sat.carstore.demo.security.jwt.JWTConfigurer;
import com.sat.carstore.demo.security.jwt.TokenProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration {

    private final TokenProvider tokenProvider;

    public SecurityConfiguration(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.headers().frameOptions().disable();

        // @formatter:off
        http.headers().frameOptions().disable().and()
                .csrf()
                .ignoringAntMatchers("/h2-console/**")
                .disable()
                .cors().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/api/login").permitAll()
                .antMatchers("/api/batch-create-users").permitAll()
                .antMatchers("/api/create-user").permitAll()
                .antMatchers("/api/list-users").hasAuthority(AuthoritiesConstants.ADMIN)
                .antMatchers("/api/list-cars").hasAnyAuthority(AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER)
                .antMatchers("/api/batch-create-cars").hasAuthority(AuthoritiesConstants.ADMIN)
                .antMatchers("/api/create-car").hasAuthority(AuthoritiesConstants.ADMIN)
                .antMatchers("/api/buy-car").hasAnyAuthority(AuthoritiesConstants.USER)
                .antMatchers("/api/history-log").hasAnyAuthority(AuthoritiesConstants.ADMIN, AuthoritiesConstants.USER)
                .antMatchers("/api/**").authenticated()
                .and()
                .httpBasic()
                .and()
                .apply(securityConfigurerAdapter());
        return http.build();
        // @formatter:on
    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }
}
