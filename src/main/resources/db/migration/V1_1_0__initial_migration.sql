CREATE TABLE authority
(
    name VARCHAR(50) NOT NULL,
    CONSTRAINT pk_authority PRIMARY KEY (name)
);

CREATE TABLE car
(
    id                 BIGINT AUTO_INCREMENT NOT NULL,
    name               VARCHAR(255)          NOT NULL,
    category           VARCHAR(255)          NOT NULL,
    brand              VARCHAR(255)          NOT NULL,
    description        CLOB,
    price              DOUBLE                NOT NULL,
    manufacturing_date date                  NOT NULL,
    owner_id           BIGINT,
    buy_date           date,
    created_by_id      BIGINT,
    CONSTRAINT pk_car PRIMARY KEY (id)
);

CREATE TABLE store_user
(
    id             BIGINT AUTO_INCREMENT NOT NULL,
    password_hash  VARCHAR(60)           NOT NULL,
    first_name     VARCHAR(50),
    last_name      VARCHAR(50),
    email          VARCHAR(254),
    budget         DOUBLE                NOT NULL,
    authority_name VARCHAR(50),
    CONSTRAINT pk_store_user PRIMARY KEY (id)
);

ALTER TABLE store_user
    ADD CONSTRAINT uc_store_user_email UNIQUE (email);

ALTER TABLE car
    ADD CONSTRAINT FK_CAR_ON_CREATEDBY FOREIGN KEY (created_by_id) REFERENCES store_user (id);

ALTER TABLE car
    ADD CONSTRAINT FK_CAR_ON_OWNER FOREIGN KEY (owner_id) REFERENCES store_user (id);

ALTER TABLE store_user
    ADD CONSTRAINT FK_STORE_USER_ON_AUTHORITY_NAME FOREIGN KEY (authority_name) REFERENCES authority (name);

INSERT INTO authority(name) VALUES ('ROLE_ADMIN');
INSERT INTO authority(name) VALUES ('ROLE_USER');
INSERT INTO store_user(password_hash,first_name,last_name,email,budget, authority_name) VALUES('$2a$10$zG.QQVtOM7CR1ExbSwEQBOYkazcgbdOUAyqEsvTr2a.G3FJFeSj3u','admin','admin','admin@carstore.com',0.0,'ROLE_ADMIN');
INSERT INTO store_user(password_hash,first_name,last_name,email,budget, authority_name) VALUES('$2a$10$c3d4C58wue3E4z9XzgogOO3zSop01EStWZRu3vTSDZhnRP43sCg7u','user','user','user@carstore.com',123456789.0,'ROLE_USER');

