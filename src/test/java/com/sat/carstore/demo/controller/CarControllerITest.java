package com.sat.carstore.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sat.carstore.demo.IntegrationTest;
import com.sat.carstore.demo.controller.dto.CarDTO;
import com.sat.carstore.demo.controller.dto.LoginDTO;
import com.sat.carstore.demo.domain.Car;
import com.sat.carstore.demo.domain.CarBrand;
import com.sat.carstore.demo.domain.CarCategory;
import com.sat.carstore.demo.repository.CarRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.sql.Date;
import java.time.Instant;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CarController} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CarControllerITest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";

    private static final CarCategory DEFAULT_CATEGORY = CarCategory.SUV;

    private static final CarBrand DEFAULT_BRAND = CarBrand.BMW;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";

    private static final Double DEFAULT_PRICE = 1D;

    private static final Date DEFAULT_MANUFACTURING_DATE = new Date(Instant.ofEpochMilli(0L).toEpochMilli());

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private MockMvc restCarMockMvc;

    private Car car;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Car createEntity() {
        Car car = new Car();
        car.setName(DEFAULT_NAME);
        car.setCategory(DEFAULT_CATEGORY);
        car.setBrand(DEFAULT_BRAND);
        car.setDescription(DEFAULT_DESCRIPTION);
        car.setPrice(DEFAULT_PRICE);
        car.setManufacturingDate(DEFAULT_MANUFACTURING_DATE);
        return car;
    }

    @BeforeEach
    public void initTest() {
        car = createEntity();
    }

    @Test
    @Transactional
    void createCar() throws Exception {
        int databaseSizeBeforeCreate = carRepository.findAll().size();
        // Create the Car
        CarDTO carDTO = CarDTO.from(car);

        LoginDTO login = new LoginDTO();
        login.setEmail("admin@carstore.com");
        login.setPassword("admin");

        String jsonToken = restCarMockMvc
                .perform(post("/api/login").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        AuthController.JWTToken token = mapper.readValue(jsonToken, AuthController.JWTToken.class);

        restCarMockMvc
            .perform(post("/api/create-car").header("Authorization", "Bearer "+token.getToken()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(carDTO)));

        // Validate the Car in the database
        List<Car> carList = carRepository.findAll();
        assertThat(carList).hasSize(databaseSizeBeforeCreate + 1);
        Car testCar = carList.get(carList.size() - 1);
        assertThat(testCar.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCar.getCategory()).isEqualTo(DEFAULT_CATEGORY);
        assertThat(testCar.getBrand()).isEqualTo(DEFAULT_BRAND);
        assertThat(testCar.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCar.getManufacturingDate().toString()).isEqualTo(DEFAULT_MANUFACTURING_DATE.toString());
        assertThat(testCar.getOwner()).isNull();

    }

    @Test
    @Transactional
    void createCarWithExistingId() throws Exception {
        // Create the Car with an existing ID
        car.setId(1L);
        CarDTO carDTO = CarDTO.from(car);

        int databaseSizeBeforeCreate = carRepository.findAll().size();

        LoginDTO login = new LoginDTO();
        login.setEmail("admin@carstore.com");
        login.setPassword("admin");
        // An entity with an existing ID cannot be created, so this API call must fail
        String jsonToken = restCarMockMvc
                .perform(post("/api/login").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        AuthController.JWTToken token = mapper.readValue(jsonToken, AuthController.JWTToken.class);

        restCarMockMvc
                .perform(post("/api/create-car").header("Authorization", "Bearer "+token.getToken()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(carDTO)))
                .andExpect(status().isBadRequest());

        // Validate the Car in the database
        List<Car> carList = carRepository.findAll();
        assertThat(carList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCars() throws Exception {
        // Initialize the database
        carRepository.saveAndFlush(car);

        // Get all the carList
        restCarMockMvc
            .perform(get("/api/list-cars"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(car.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].category").value(hasItem(DEFAULT_CATEGORY.toString())))
            .andExpect(jsonPath("$.[*].brand").value(hasItem(DEFAULT_BRAND.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.doubleValue())))
            .andExpect(jsonPath("$.[*].manufacturingDate").value(hasItem(DEFAULT_MANUFACTURING_DATE.toString())));
    }

    @Test
    @Transactional
    void buyExistingCar() throws Exception {
        // Initialize the database
        carRepository.saveAndFlush(car);

        int databaseSizeBeforeUpdate = carRepository.findAll().size();

        CarDTO carDTO = CarDTO.from(car);

        LoginDTO login = new LoginDTO();
        login.setEmail("user@carstore.com");
        login.setPassword("user");

        String jsonToken = restCarMockMvc
                .perform(post("/api/login").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        AuthController.JWTToken token = mapper.readValue(jsonToken, AuthController.JWTToken.class);

        restCarMockMvc
            .perform(put("/api/buy-car").header("Authorization", "Bearer "+token.getToken()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(carDTO)))
            .andExpect(status().isOk());

        // Validate the Car in the database
        List<Car> carList = carRepository.findAll();
        assertThat(carList).hasSize(databaseSizeBeforeUpdate);
        Car testCar = carList.get(carList.size() - 1);
        assertThat(testCar.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCar.getCategory()).isEqualTo(DEFAULT_CATEGORY);
        assertThat(testCar.getBrand()).isEqualTo(DEFAULT_BRAND);
        assertThat(testCar.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCar.getManufacturingDate().toString()).isEqualTo(DEFAULT_MANUFACTURING_DATE.toString());
        assertThat(testCar.getOwner().getEmail()).isEqualTo(login.getEmail());

    }

    @Test
    @Transactional
    void buyNonExistingCar() throws Exception {
        int databaseSizeBeforeUpdate = carRepository.findAll().size();
        car.setId(count.incrementAndGet());

        CarDTO carDTO = CarDTO.from(car);

        LoginDTO login = new LoginDTO();
        login.setEmail("user@carstore.com");
        login.setPassword("user");

        String jsonToken = restCarMockMvc
                .perform(post("/api/login").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        AuthController.JWTToken token = mapper.readValue(jsonToken, AuthController.JWTToken.class);

        restCarMockMvc
                .perform(put("/api/buy-car").header("Authorization", "Bearer "+token.getToken()).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(carDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Car in the database
        List<Car> carList = carRepository.findAll();
        assertThat(carList).hasSize(databaseSizeBeforeUpdate);
    }

}
