package com.sat.carstore.demo.controller;

import com.sat.carstore.demo.IntegrationTest;
import com.sat.carstore.demo.controller.dto.LoginDTO;
import com.sat.carstore.demo.domain.StoreUser;
import com.sat.carstore.demo.repository.AuthorityRepository;
import com.sat.carstore.demo.repository.StoreUserRepository;
import com.sat.carstore.demo.security.AuthoritiesConstants;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AuthController} REST controller.
 */
@AutoConfigureMockMvc
@IntegrationTest
class AuthControllerITest {

    @Autowired
    private StoreUserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Transactional
    void testAuthorize() throws Exception {
        StoreUser user = new StoreUser();
        user.setEmail("user-jwt-controller@example.com");
        user.setPassword(passwordEncoder.encode("test"));
        user.setBudget(0D);
        user.setFirstName("xxx");
        user.setLastName("yyy");
        user.setAuthority(authorityRepository.findById(AuthoritiesConstants.USER).orElse(null));

        userRepository.saveAndFlush(user);

        LoginDTO login = new LoginDTO();
        login.setEmail("user-jwt-controller@example.com");
        login.setPassword("test");
        mockMvc
            .perform(post("/api/login").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.token").isString())
            .andExpect(jsonPath("$.token").isNotEmpty())
            .andExpect(header().string("Authorization", not(nullValue())))
            .andExpect(header().string("Authorization", not(is(emptyString()))));
    }


    @Test
    void testAuthorizeFails() throws Exception {
        LoginDTO login = new LoginDTO();
        login.setEmail("wrong-user@example.com");
        login.setPassword("wrong password");
        mockMvc
            .perform(post("/api/login").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.token").doesNotExist())
            .andExpect(header().doesNotExist("Authorization"));
    }
}
