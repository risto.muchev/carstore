package com.sat.carstore.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sat.carstore.demo.IntegrationTest;
import com.sat.carstore.demo.controller.dto.LoginDTO;
import com.sat.carstore.demo.controller.dto.PasswordStoreUserDTO;
import com.sat.carstore.demo.controller.dto.StoreUserDTO;
import com.sat.carstore.demo.domain.Authority;
import com.sat.carstore.demo.domain.StoreUser;
import com.sat.carstore.demo.repository.AuthorityRepository;
import com.sat.carstore.demo.repository.StoreUserRepository;
import com.sat.carstore.demo.security.AuthoritiesConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StoreUserController} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class StoreUserControllerITest {

    private static final Long DEFAULT_ID = 1L;

    private static final String DEFAULT_PASSWORD = "passjohndoe";

    private static final String DEFAULT_EMAIL = "johndoe@localhost";

    private static final String DEFAULT_FIRSTNAME = "john";

    private static final String DEFAULT_LASTNAME = "doe";

    private static final Double DEFAULT_BUGGET = 12345678900D;

    @Autowired
    private StoreUserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    private MockMvc restUserMockMvc;

    private StoreUser user;

    /**
     * Create a User.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which has a required relationship to the User entity.
     */
    public static StoreUser createEntity() {
        StoreUser user = new StoreUser();
        user.setPassword(String.valueOf(Math.random()*1000));
        user.setEmail(Math.random()*1000 + DEFAULT_EMAIL);
        user.setFirstName(DEFAULT_FIRSTNAME);
        user.setLastName(DEFAULT_LASTNAME);
        user.setBudget(DEFAULT_BUGGET);
        return user;
    }

    /**
     * Setups the database with one user.
     */
    public static StoreUser initTestUser(StoreUserRepository userRepository) {
        StoreUser user = createEntity();
        user.setEmail(DEFAULT_EMAIL);
        return user;
    }

    @BeforeEach
    public void initTest() {
        user = initTestUser(userRepository);
        user.setAuthority(authorityRepository.findById(AuthoritiesConstants.USER).orElse(null));
    }

    @Test
    @Transactional
    void createUser() throws Exception {
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        // Create the User
        PasswordStoreUserDTO managedUserVM = new PasswordStoreUserDTO();
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail(DEFAULT_EMAIL);
        managedUserVM.setBudget(DEFAULT_BUGGET);
        managedUserVM.setAuthority(AuthoritiesConstants.USER);

        restUserMockMvc
            .perform(
                post("/api/create-user").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(managedUserVM))
            )
            .andExpect(status().isOk());

        // Validate the User in the database
        assertPersistedUsers(users -> {
            assertThat(users).hasSize(databaseSizeBeforeCreate + 1);
            StoreUser testUser = users.get(users.size() - 1);
            assertThat(testUser.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
            assertThat(testUser.getLastName()).isEqualTo(DEFAULT_LASTNAME);
            assertThat(testUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        });
    }

    @Test
    @Transactional
    void createUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        PasswordStoreUserDTO managedUserVM = new PasswordStoreUserDTO();
        managedUserVM.setId(DEFAULT_ID);
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail(DEFAULT_EMAIL);
        managedUserVM.setBudget(DEFAULT_BUGGET);
        managedUserVM.setAuthority(AuthoritiesConstants.USER);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserMockMvc
            .perform(
                post("/api/create-user").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(managedUserVM))
            )
            .andExpect(status().isBadRequest());

        // Validate the User in the database
        assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeCreate));
    }

    @Test
    @Transactional
    void createUserWithExistingEmail() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);
        int databaseSizeBeforeCreate = userRepository.findAll().size();

        PasswordStoreUserDTO managedUserVM = new PasswordStoreUserDTO();
        managedUserVM.setPassword(DEFAULT_PASSWORD);
        managedUserVM.setFirstName(DEFAULT_FIRSTNAME);
        managedUserVM.setLastName(DEFAULT_LASTNAME);
        managedUserVM.setEmail(DEFAULT_EMAIL); // this email should already be used
        managedUserVM.setBudget(DEFAULT_BUGGET);
        managedUserVM.setAuthority(AuthoritiesConstants.USER);

        // Create the User
        restUserMockMvc
            .perform(
                post("/api/create-user").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(managedUserVM))
            )
            .andExpect(status().isBadRequest());

        // Validate the User in the database
        assertPersistedUsers(users -> assertThat(users).hasSize(databaseSizeBeforeCreate));
    }

    @Test
    @Transactional
    void getAllUsers() throws Exception {
        // Initialize the database
        userRepository.saveAndFlush(user);

        LoginDTO login = new LoginDTO();
        login.setEmail("admin@carstore.com");
        login.setPassword("admin");
        // An entity with an existing ID cannot be created, so this API call must fail
        String jsonToken = restUserMockMvc
                .perform(post("/api/login").contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(login)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        ObjectMapper mapper = new ObjectMapper();
        AuthController.JWTToken token = mapper.readValue(jsonToken, AuthController.JWTToken.class);

        // Get all the users
        restUserMockMvc
            .perform(get("/api/list-users").header("Authorization","Bearer "+token.getToken()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRSTNAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LASTNAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));
    }

    @Test
    void testUserEquals() throws Exception {
        TestUtil.equalsVerifier(StoreUser.class);
        StoreUser user1 = new StoreUser();
        user1.setId(DEFAULT_ID);
        StoreUser user2 = new StoreUser();
        user2.setId(user1.getId());
        assertThat(user1).isEqualTo(user2);
        user2.setId(2L);
        assertThat(user1).isNotEqualTo(user2);
        user1.setId(null);
        assertThat(user1).isNotEqualTo(user2);
    }

    @Test
    void testUserToUserDTO() {
        user.setId(DEFAULT_ID);
        Authority authority = new Authority();
        authority.setName(AuthoritiesConstants.USER);
        user.setAuthority(authority);

        StoreUserDTO userDTO = new StoreUserDTO(user);

        assertThat(userDTO.getId()).isEqualTo(DEFAULT_ID);
        assertThat(userDTO.getFirstName()).isEqualTo(DEFAULT_FIRSTNAME);
        assertThat(userDTO.getLastName()).isEqualTo(DEFAULT_LASTNAME);
        assertThat(userDTO.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(userDTO.getAuthority()).isEqualToIgnoringCase(AuthoritiesConstants.USER);
        assertThat(userDTO.toString()).isNotNull();
    }

    @Test
    void testAuthorityEquals() {
        Authority authorityA = new Authority();
        assertThat(authorityA).isNotEqualTo(null).isNotEqualTo(new Object());
        assertThat(authorityA.hashCode()).isZero();
        assertThat(authorityA.toString()).isNotNull();

        Authority authorityB = new Authority();
        assertThat(authorityA).isEqualTo(authorityB);

        authorityB.setName(AuthoritiesConstants.ADMIN);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityA.setName(AuthoritiesConstants.USER);
        assertThat(authorityA).isNotEqualTo(authorityB);

        authorityB.setName(AuthoritiesConstants.USER);
        assertThat(authorityA).isEqualTo(authorityB).hasSameHashCodeAs(authorityB);
    }

    private void assertPersistedUsers(Consumer<List<StoreUser>> userAssertion) {
        userAssertion.accept(userRepository.findAll());
    }
}
