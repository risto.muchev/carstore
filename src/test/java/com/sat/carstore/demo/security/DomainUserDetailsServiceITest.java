package com.sat.carstore.demo.security;

import com.sat.carstore.demo.IntegrationTest;
import com.sat.carstore.demo.domain.StoreUser;
import com.sat.carstore.demo.repository.AuthorityRepository;
import com.sat.carstore.demo.repository.StoreUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.Transactional;
import com.sat.carstore.demo.service.DomainUserDetailsService;

import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integrations tests for {@link DomainUserDetailsService}.
 */
@Transactional
@IntegrationTest
class DomainUserDetailsServiceITest {

    private static final String USER_ONE_EMAIL = "test-user-one@localhost";
    private static final String USER_TWO_EMAIL = "test-user-two@localhost";
    private static final String USER_THREE_EMAIL = "test-user-three@localhost";

    @Autowired
    private StoreUserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @Autowired
    @Qualifier("userDetailsService")
    private UserDetailsService domainUserDetailsService;

    @BeforeEach
    public void init() {
        StoreUser userOne = new StoreUser();
        userOne.setPassword("abcd");
        userOne.setEmail(USER_ONE_EMAIL);
        userOne.setFirstName("userOne");
        userOne.setLastName("doe");
        userOne.setAuthority(authorityRepository.findById(AuthoritiesConstants.USER).orElse(null));
        userRepository.save(userOne);

        StoreUser userTwo = new StoreUser();
        userTwo.setPassword("efgh");
        userTwo.setEmail(USER_TWO_EMAIL);
        userTwo.setFirstName("userTwo");
        userTwo.setLastName("doe");
        userTwo.setAuthority(authorityRepository.findById(AuthoritiesConstants.USER).orElse(null));
        userRepository.save(userTwo);

        StoreUser userThree = new StoreUser();
        userThree.setPassword("ijklm");
        userThree.setEmail(USER_THREE_EMAIL);
        userThree.setFirstName("userThree");
        userThree.setLastName("doe");
        userThree.setAuthority(authorityRepository.findById(AuthoritiesConstants.USER).orElse(null));
        userRepository.save(userThree);
    }

    @Test
    void assertThatUserCanBeFoundByEmail() {
        UserDetails userDetails = domainUserDetailsService.loadUserByUsername(USER_TWO_EMAIL);
        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(USER_TWO_EMAIL);
    }

    @Test
    void assertThatUserCanBeFoundByEmailIgnoreCase() {
        UserDetails userDetails = domainUserDetailsService.loadUserByUsername(USER_TWO_EMAIL.toUpperCase(Locale.ENGLISH));
        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(USER_TWO_EMAIL);
    }

    @Test
    void assertThatEmailIsPrioritizedOverLogin() {
        UserDetails userDetails = domainUserDetailsService.loadUserByUsername(USER_ONE_EMAIL);
        assertThat(userDetails).isNotNull();
        assertThat(userDetails.getUsername()).isEqualTo(USER_ONE_EMAIL);
    }
}
