# Carstore solution guide

### Techologies used for the solution
For further reference, please consider the following sections:

* [Apache Maven](https://maven.apache.org/guides/index.html) - Build tool
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.7.6/reference/htmlsingle/#web) - Web app development
* [Spring Security](https://docs.spring.io/spring-boot/docs/2.7.6/reference/htmlsingle/#web.security) - Authentication and authorization, User Roles, etc
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.7.6/reference/htmlsingle/#data.sql.jpa-and-spring-data) - Persistance layer - Hibernate ORM
* [Flyway Migration](https://docs.spring.io/spring-boot/docs/2.7.6/reference/htmlsingle/#howto.data-initialization.migration-tool.flyway) - Database migrations
* [H2](https://www.h2database.com/html/main.html) - Database
* [Lombok](https://projectlombok.org) - Logging (Slf4j), remove boilerplate code
* [Jsonwebtoken](https://jwt.io) - JWT token
* [javax.validation](https://beanvalidation.org/) - Validation

### Installation

#### Clone the project from git:
git clone https://gitlab.com/risto.muchev/carstore

#### Build command without running tests:
mvn clean install -DskipTests=true

#### Run commands:
cd target<br />
java -jar demo-0.0.1-SNAPSHOT.jar

### Guide

H2 database (with file storage) should be present in /target/carstoredb.mv.db

You can access it on this url: http://localhost:8080/h2-console/
and log in with the following credentials:
![Screenshot](h2.png)

There are 2 pre-created users in the database with credentials:
1. email: "admin@carstore.com" password: "admin" role: "ROLE_ADMIN"
2. email: "user@carstore.com" password: "user" role: "ROLE_USER"

Some of the API endpoints require "Authorization" header present in the request. This is how the application knows if the user is valid and what role it has.<br /><br /> To get the token you need to login using this API endpoint: POST http://localhost:8080/api/login with body: {"email":"admin@carstore.com","password":"admin"} and use the response {
"token": "from.backend.abc"
} as header "Authorization": "Bearer 'token'" in the future request for the endpoints that require it.

#### API Endpoints

You can use the POSTMAN collection [CarStore.postman_collection.json] file where all API endpoints are present.

The following API endpoints are exposed:

1. GET http://localhost:8080/h2-console - Does NOT require Authorization
2. POST http://localhost:8080/api/login - Does NOT require Authorization<br />
   Example body: {"email":"admin@carstore.com","password":"admin"}
3. POST http://localhost:8080/api/create-user - Does NOT require Authorization, but only ROLE_ADMIN user can create other ROLE_ADMIN users <br />
   Example body: {"firstName":"Risto","lastName":"Muchev","email":"rm@carstore.com","password":"RistoM", "budget":999999.0, "authority":"ROLE_USER"}
4. POST http://localhost:8080/api/batch-create-users - Does NOT require Authorization, but only ROLE_ADMIN user can create other ROLE_ADMIN users <br />
   Example body: [{"firstName":"u1","lastName":"u1","email":"u1@carstore.com","password":"u1", "budget":11111.0, "authority":"ROLE_USER"},
   {"firstName":"u2","lastName":"u2","email":"u2@carstore.com","password":"u2", "budget":22222.0, "authority":"ROLE_USER"}]
5. GET http://localhost:8080/api/list-users - requires Authorization as ROLE_ADMIN user
6. GET http://localhost:8080/api/list-cars - requires Authorization as ROLE_USER or ROLE_ADMIN user
7. POST http://localhost:8080/api/batch-create-cars - requires Authorization as ROLE_ADMIN user <br />
   Example body: [{"name":"car name1", "category":"SUV", "brand":"BMW", "description":"Car description1", "price":100, "manufacturingDate":"2022-01-01"}, {"name":"car name2", "category":"LIMO", "brand":"FIAT", "description":"Car description2", "price":200, "manufacturingDate":"2022-02-02"}]
8. POST http://localhost:8080/api/create-car - requires Authorization as ROLE_ADMIN user <br />
   Example body: {"name":"car name", "category":"SUV", "brand":"BMW", "description":"Car description", "price":100, "manufacturingDate":"2022-03-03"}
9. PUT http://localhost:8080/api/buy-car - requires Authorization as ROLE_USER <br />
   Example body (car id): {"id":1} 
10. GET http://localhost:8080/api/history-log - requires Authorization as ROLE_USER or ROLE_ADMIN user


### Tests

#### Build command with running Unit and Integration tests:
mvn clean install

### Architecture and Coding style

#### ER diagram
![Screenshot](er.png)

### Layers
Three layered architecture is used: 
1. Controllers - Validating the request, calling multiple Services for business logic execution and returning result.
2. Services - Components for implementing the business logic, using multiple Repositores for DB operations
3. Repositories - Responsible for DB operations

### Packages
1. config - contains Configuration classes
2. controller - contains Controller classes
3. domain - contains Entity classes
4. repository - contains Repository classes
5. security - contains Security and JWT classes
6. service - contains Service classes

### DB migration
Flyway is used for DB schema initialization. The script is in /resources/db.migration/V1_1_0__initial_migration.sql

### Application properties
There is no need for separate envioriments (dev, staging, prod) so all configuration properties are in /resources/application.properties